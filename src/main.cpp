// Inclure le framework mbed


#define TX (USBTX)
#define RX (USBRX)

// Créer une instance de la classe `Serial` en passant les constantes TX et RX
// https://os.mbed.com/docs/mbed-os/v5.11/mbed-os-api-doxy/classmbed_1_1_serial.html#adbd0b169f57aa84c060ca0e21971af9f


int main() {
	// Ajustez la vitesse (baud) de votre instance à 115200
	// https://os.mbed.com/docs/mbed-os/v5.11/mbed-os-api-doxy/classmbed_1_1_serial_base.html#a9afb7aa9321cd71a8a26a673157583d2

	// Écrivez le message "Hello World!\n" sur le port UART (utilisez la méthode
	// `printf()`)

	const uint8_t x = 1;

	// Écrivez la variable `x` sur le port UART
	// http://www.cplusplus.com/reference/cstdio/printf/

}

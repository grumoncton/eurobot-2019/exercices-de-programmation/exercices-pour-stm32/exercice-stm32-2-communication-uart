<!--
vim: spelllang=fr keymap=accents
-->
# Exercice 2 de programmation pour STM32 : Communication UART

Ce deuxième exercice vous introduira à [la communication UART (Universal
Asynchronous Receiver-Transmitter)][uart]. Ce protocole est communément
(incorrectement) appelé "communication serial".

[uart]: https://en.wikipedia.org/wiki/Universal_asynchronous_receiver-transmitter

## Marches à suivre

### Téléchargement
Exécutez les commandes suivantes dans *Git Bash*.
1. Créez un dossier pour le répertoire:
	```
	mkdir -p ~/code/grum/eurobot2019/exercices/exercices-pour-stm32
	```
1. Clonez le répertoire:
	```
	git clone ssh://gitlab@gitlab.robitaille.host:49/grum/eurobot2019/exercices/exercices-pour-stm32/exercice-stm32-2-communication-uart.git ~/code/grum/eurobot2019/exercices/exercices-pour-stm32/exercice-stm32-2-communication-uart
	```
1. Ouvrez le dossier:
	```
	cd ~/code/grum/eurobot2019/exercices/exercices-pour-stm32/exercice-stm32-2-communication-uart
	```
1. Ouvrez le dossier dans *Visual Sutdio Code*:
	```
	code .
	```

### Programmation
Ouvrez le fichier `src/main.cpp` et ajoutez les instructions manquantes (sous
les commentaires).

### Compilation
Une fois que le code est écrit, compiler le programme (<kbd>ctrl-shift-p</kbd>,
`PlatformIO: Build` ou <kbd>ctrl-alt-b</kbd> dans *Visual Studio Code*).

### Téléversement
Si vous avez accès à un microcontrôleur (on en a commandé!), télé versez le
programme compilé avec <kbd>ctrl-shift-p</kbd> et `PlatformIO: Upload` ou
<kbd>ctrl-alt-u</kbd>.
